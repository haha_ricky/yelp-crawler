<?php
/**
 * Usage: In terminal, run the following command for first run
 * sudo /var/www/app/Console/cake -app /var/www/app crawler
 *
 * You can run the following optional functions
 * - init : To scrap business categories and its child categories
 *      sudo /var/www/app/Console/cake -app /var/www/app crawler init (parent category) (depth)
 * - addBusiness : To scrap businesses by the category id
 *      sudo /var/www/app/Console/cake -app /var/www/app crawler addBusiness (from category id) (to category id)
 * - updateLatLng : To update geocode of the business by address
 *      sudo /var/www/app/Console/cake -app /var/www/app crawler updateLatLng (from business id) (to business id)
 *
 * User: ricky.chow
 * Date: 28/7/15
 */


App::uses('phpQuery','Vendor');
App::uses('customCurl','Vendor');
App::uses('GoogleMap','Vendor');
class CrawlerShell extends AppShell {

    public $uses = array('Category', 'Business');

    public function _welcome() {

    }

    public function __construct($stdout = null, $stderr = null, $stdin = null) {
        parent::__construct($stdout, $stderr, $stdin);
        $this->_useLogger(false);
    }

    /**
     * Core function
     */
    public function main() {
        $memoryLimit = ini_get('memory_limit');
        ini_set('memory_limit', '1024M');
        $baseURL[0] = "/search?find_loc=New+York,+NY";

        $this->init($baseURL, 0);
        $this->addBusiness();
        $this->updateLatLng();
        ini_set('memory_limit', $memoryLimit);

    }

    /**
     * * CURL HTML from page.
     * @param $url
     * @return bool|mixed
     */

    public function getHtml($url) {
        $count = 0;
        $max_tries = 5;
        $success = true;
        $ch = new customCurl();
        do{
            $html = $ch->getHtml($url);
            if($ch->isError()){
                debug("==== Fail to retrieve data: $count/$max_tries ===");
                sleep(rand(1,5));
            }
            $count++;
            if($count >= $max_tries){
                $success = false;
                break;
            }
        }while($ch->isError());
        if(!$success){
            return false;
        }
        return $html;
    }

    /**
     * Deep scrap the exact link of the page and save results to database
     * @param $categoryLink
     * @param $depth
     * @return array
     */
    public function getSubCategoryLinksByHtml($categoryLink, $depth){
        $subCategoryLinks = array();
        $subCategoryName = array();
        $html = $this->getHtml(Configure::read("DomainName").$categoryLink);

        if(!empty($html)) {
            $doc = phpQuery::newDocumentHTML($html);
            foreach ($doc['div.js-all-category-browse-links a.category-browse-anchor'] as $catLink) {
                $subCategoryLinks[] .= pq($catLink)->attr('href');
                $subCategoryName[] .= pq($catLink)->text();
            }
            foreach ($doc['div.js-top-category-browse-links a.category-browse-anchor'] as $catLink) {
                $subCategoryLinks[] .= pq($catLink)->attr('href');
                $subCategoryName[] .= pq($catLink)->text();
            }
            $subCategoryLinks = array_unique($subCategoryLinks);

            //save all links to DB
            $subCategoryData = array();

            $parent = $this->Category->find('first', ['conditions'=>['Category.link' => $categoryLink]]);
            foreach($subCategoryLinks as $key => $sl){
                $subCategoryData[$key]['Category']['name'] = $subCategoryName[$key];
                $subCategoryData[$key]['Category']['link'] = $sl;
                $subCategoryData[$key]['Category']['depth'] = $depth;
                empty($parent)? null : $subCategoryData[$key]['Category']['parent_id'] = $parent['Category']['id'];
            }
            $this->Category->saveAll($subCategoryData);
            unset($doc);
        }
        return $subCategoryLinks;
    }

    /**
     * Recursively scrape subcategory links
     * @param $urls
     * @param $depth
     */
    public function init($urls, $depth){
        foreach($urls as $url){
            $subCats = $this->getSubCategoryLinksByHtml($url, $depth);
            $depth ++;
            if(!empty($subCats)){
                if($depth < 2){
                    $this->init($subCats, $depth);
                }
            }
            $depth--;
        }
    }

    /**
     * Update businesses
     * Can enter range of category id when triggered.
     * i.e: sudo /var/www/app/Console/cake -app /var/www/app crawler update 24 24
     * Or keep range of category to empty for all businesses
     * i.e: sudo /var/www/app/Console/cake -app /var/www/app crawler update
     */

    public function addBusiness(){
        if(isset($this->args[0]) && isset($this->args[1])){
            $categories = $this->Category->find('all', ['conditions'=>['Category.id >=' => (int)$this->args[0], 'Category.id <=' => (int)$this->args[1]]]);
        }else{
            $categories = $this->Category->find('all', ['conditions'=>['Category.parent_id >' => 0]]);
        }

        foreach ($categories as $cat){
            $start = 0;
            $ch = new customCurl();

            do{
                $business = array();
                $businessName = array();
                $businessLink = array();
                $businessImageLink = array();
                $businessZipCode = array();
                $businessPhone = array();
                $businessAddress = array();

                usleep(rand(500000,2000000)); //sleep for a while to advoid ban
                $html = $ch->getHtml(Configure::read("DomainName"). $cat['Category']['link']. "&start=". $start);
                $doc = phpQuery::newDocumentHTML($html);

                if($doc['li.regular-search-result a.biz-name']->text() == null){
                    break;
                }else{
                    foreach ($doc['li.regular-search-result a.biz-name'] as $name) {
                        $businessName[] = pq($name)->text();
                    }
                    foreach ($doc['li.regular-search-result a.biz-name'] as $link) {
                        $businessLink[] = pq($link)->attr('href');
                    }
                    foreach ($doc['li.regular-search-result img.photo-box-img'] as $image) {
                        $img = str_replace("//","",pq($image)->attr('src'));
                        $businessImageLink[] = $img;
                    }

                    foreach ($doc['li.regular-search-result address'] as $address) {
                        //format address
                        $addr = preg_replace('/^\s+|\n|\r|\s+$/m',' ',pq($address)->text());
                        $re = '/(?)
                        (?<=[a-z])
                        (?=[A-Z])
                        | (?<=[A-Z])
                        (?=[A-Z][a-z])
                        /x';
                        $a = preg_split($re, $addr);
                        $count = count($a);
                        $formattedAddress = "";
                        for ($i = 0; $i < $count; ++$i) {
                            $formattedAddress .= $a[$i] . " ";
                        }

                        $businessAddress[] = $formattedAddress;
                        //get zip code
                        $ex = explode(" ", $formattedAddress);
                        $businessZipCode[] = is_numeric(end($ex))?  end($ex) : null;
                    }
                    foreach ($doc['li.regular-search-result span.biz-phone'] as $phone){
                        $phone = preg_replace('/^\s+|\n|\r|\s+$/m','',pq($phone)->text());
                        $businessPhone[] = !empty($phone) ? $phone : null;
                    }

                    foreach($businessName as $key => $b){
                        $business[$key]['category_id'] = $cat['Category']['id'];
                        $business[$key]['name'] = $businessName[$key];
                        $business[$key]['link'] = $businessLink[$key];
                        $business[$key]['address'] = $businessAddress[$key];
                        $business[$key]['zip'] = $businessZipCode[$key];
                        $business[$key]['phone'] = $businessPhone[$key];
                        $business[$key]['imageLink'] = $businessImageLink[$key];
                    }

                    debug($business);
                    $this->Business->saveAll($business);
                }
                $start+=10;
            }while($start < 1000);
        }
    }

    public function updateLatLng(){
        if(isset($this->args[0]) && isset($this->args[1])){
            $businesses = $this->Business->find('all', ['conditions'=>['Business.id >=' => (int)$this->args[0], 'Business.id <=' => (int)$this->args[1]]]);
        }else{
            $businesses = $this->Business->find('all', ['conditions'=>['Business.Business >' => 0]]);
        }

        $googleMap = new GoogleMap();
        foreach ($businesses as $key => $business) {
            $locationInfo = $googleMap->geocode($business['Business']['address']);
            if ($locationInfo) {
                $businesses[$key]['Business']['lat'] = $locationInfo[0];
                $businesses[$key]['Business']['lng'] = $locationInfo[1];
            }
        }
        $this->Business->saveAll($businesses);
    }

}
