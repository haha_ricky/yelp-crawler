<?php

class customCurl {

    public $ch;

    /**
     * cURL Settings - Pretend to be Googlebot to avoid blocking
     */
    function __construct(){
        $this->ch = curl_init();

        $this->setopt(CURLOPT_USERAGENT,"Googlebot/2.1 (http://www.googlebot.com/bot.html)");
        $this->setopt(CURLOPT_RETURNTRANSFER, 1);
        $this->setopt(CURLOPT_SSL_VERIFYHOST, false);
        $this->setopt(CURLOPT_SSL_VERIFYPEER, false);
        $this->setopt(CURLOPT_MAXREDIRS, 10);
        $this->setopt(CURLOPT_CONNECTTIMEOUT, 5);
        $this->setopt(CURLOPT_AUTOREFERER, true);
        $this->setopt(CURLOPT_FOLLOWLOCATION, true);
        $this->setopt(CURLOPT_TIMEOUT, 2 );
    }

    function getHtml($url){
        $this->setopt(CURLOPT_URL, $url);
        $html = $this->exec();
        return $html;
    }

    function exec(){
        return curl_exec($this->ch);
    }

    function close(){
        curl_close($this->ch);
    }

    function isError(){
        return curl_errno($this->ch);
    }

    function setopt($option, $value){
        curl_setopt($this->ch, $option, $value);
    }

    public function __destruct()
    {
        if($this->ch != null){
            $this->close();
        }
    }



}
?>