<?php
/**
 * Created by PhpStorm.
 * User: Ricky
 * Date: 1/8/15
 * Time: 4:16 PM
 */

class Business extends AppModel{
    public function search ($data){
        $limit = 1000;
        $name = empty($data['name'])? null : $data['name'];
        $category_id = empty($data['category'])? null : $data['category'];
        $subcategory_id = empty($data['subcategory'])? null : $data['subcategory'];
        $zip = empty($data['zip'])? null : $data['zip'];
        if(!empty($category_id) && empty($subcategory_id)){
            $conditions = array(
                'conditions' => array(
                    'parent_id' => $category_id,
                ),
                'fields' => array(
                    'Category.id'
                ),
            );
            $subcategory_id_list = $this->Category->find('list', $conditions);
            $conditions = array(
                'conditions' => array(
                    'category_id' => $subcategory_id_list
                ),
            );
        }else if(empty($category_id) && empty($subcategory_id)){
            $conditions = array(
                'conditions' => array()
            );
        }
        else{
            $conditions = array(
                'conditions' => array(
                    'category_id' => $subcategory_id
                ),
            );
        }
        if(!empty($zip)){
            $conditions['conditions']['zip'] = $zip;
        }
        if(!empty($name)){
            $conditions['conditions']['name LIKE'] = '%'.$name.'%';
        }
        $conditions['limit'] = $limit;
        return $this->find('all', $conditions);
    }
}