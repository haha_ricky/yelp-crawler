<?php
/**
 * User: ricky.chow
 * Date: 31/7/15
 * Time: 4:37 PM
 */

class Category extends AppModel {
    public $hasMany = array(
        'Business' => array(
            'className' => 'Business',
            'foreignKey' => 'category_id',
        )
    );
}