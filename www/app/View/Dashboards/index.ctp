<div class="div_left_panel">
    <div class="div_search_panel">

        <div class="form-group">
            <?php echo $this->Form->input('Dashboard.name', array(
                'label' => 'Name',
                'class' => 'form-control'
            )); ?>
        </div>

        <div class="form-group">
            <?php
            echo $this->Form->input('Dashboard.category', array(
                    'options' => $categoryList,
                    'empty' => ' == Please select == ',
                    'class'=> 'form-control',
                    'label' => 'Category'
                )
            );
            ?>
        </div>
        <div class="form-group">
            <select name="data[Dashboard][Subcategory]" id ='DashboardSubcategory' class="form-control">
                <option value=''> == Please select == </option>
            </select>
        </div>

        <div class="form-group">
            <?php echo $this->Form->input('Dashboard.zip', array(
                'label' => 'Zip Code',
                'class' => 'form-control'
            )); ?>
        </div>
        <button id="search_btn" type="button" class="btn btn-default btn-block btn-primary">Search!</button>
    </div>

    <div class="div_search_result_panel">
        <div class="search_results"></div>
    </div>
</div>

<div class="div_right_panel">
    <div id="map-canvas"></div>
</div>

<script>
    var map = $('#map-canvas');
    $(document).ready(function(){
            loadResults();
        }
    );

    $('#DashboardCategory').change(function(){
        $.ajax({
            type: "POST",
            url: "Dashboards/loadSubCategoryDropdownList/"+ $('#DashboardCategory').val(),
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                $('#DashboardSubcategory').find('option').remove().end().append("<option value=''> == Please select == </option>");

                $.each(data, function () {
                    $("#DashboardSubcategory").append($("<option></option>").val(this['value']).html(this['text']));
                });
            }
        });
    });

    $('#search_btn').on('click' ,function(){
        loadResults();
    });

    function loadResults(){
        nameVal = $('#DashboardName').val();
        categoryVal = $('#DashboardCategory').val();
        subcategoryVal = $('#DashboardSubcategory').val();
        zipVal = $('#DashboardZip').val();

        var locations = [];
        $.ajax({
            type: "POST",
            url: "Dashboards/getResults/",
            data: { name: nameVal, category: categoryVal, subcategory: subcategoryVal, zip: zipVal},
            dataType: "json",

            success: function (data) {
                var _html ='';
                data.Business.forEach(function(e){
                    _html += '<div class="result_block" business-id="'+ e.Business.id+'" >' +
                        '<div class="row">' +
                        '<div class="result_block_image_control">' +
                        '<img class= "result_block_image" src = "http://'+ e.Business.imageLink + '"/>' +
                        '</div>'+
                        '<div class="result_block_info_control">'+
                        '<h5>' + e.Business.name + '</h5>'+
                        '<address class="result_address">' + e.Business.address + '<br /> <abbr title="Phone">P:</abbr>' + e.Business.phone + '</address>'+
                        '<a href="'+ data.DomainName + e.Business.link + '" target="_blank"> View in Yelp! </a>' +
                        '</div>'+
                        '</div>'+
                        '</div>';

                    var info={};
                    info.addr = [e.Business.lat, e.Business.lng];
                    info.text = e.Business.name;
                    info.id = e.Business.id;
                    locations.push(info);

                });
                if(_html.length === 0){
                    _html = "<h3>No record found!</h3>";
                }
                $('.search_results').append(_html).fadeIn(1000);
                $('.div_search_result_panel').animate({
                    scrollTop: 0
                }, 'slow');

                map.tinyMap('destroy');
                map.tinyMap({
                    'center': {'x': '40.7033127', 'y': '-73.979681'},   //New york geolocation
                    'zoom':  10,
                    'cluster': true,
                    'markerCluster': true,
                    'marker': locations
                });
            }
        });
    }

    $('.search_results').on('click', '.result_block', function () {
        var obj = $(this),
            id = $(this).attr('business-id'),
            m = {},
            marker = {},
            markers = [],
            i = 0;

        m = map.data('tinyMap');
        markers = m._markers;

        for (i; i < markers.length; i += 1) {
            marker = markers[i];
            marker.infoWindow.close();
            console.dir(typeof marker.infoWindow.close);
            if (id === marker.id) {
                marker.infoWindow.open(m.map, marker);
                m.map.panTo(marker.position);
                m.map.setZoom(18);

            }
        }
    });
</script>
