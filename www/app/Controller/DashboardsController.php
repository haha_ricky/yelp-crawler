<?php
/**
 * Dashboard Controller
 * User: Ricky
 * Date: 2/8/15
 * Time: 11:48 PM
 */

App::uses('GoogleMap','Vendor');
class DashboardsController extends AppController{

    public function __construct( $request = null, $response = null ) {
        parent::__construct( $request, $response );
        $this->loadModel('Business');
        $this->loadModel('Category');
    }
    public function index(){
        $this->set('categoryList', $this->loadCategoryDropdownList());
    }

    /**
     * Load category to drop down list
     * @return mixed
     */
    public function loadCategoryDropdownList(){
        $conditions = array(
            'conditions'=> array(
                'Category.parent_id' => null
            )
        );
        return $this->Category->find('list', $conditions);
    }

    /**
     * Load subcategory to dropdown list by parent category ID
     * @param $parentCatId
     * @return string
     */
    public function loadSubCategoryDropdownList($parentCatId){
        $this->autoRender = false;

        $conditions = array(
            'conditions'=> array(
                'Category.parent_id' => $parentCatId
            ),
            'fields'=> array(
                'id', 'name'
            )
        );
        $results = $this->Category->find('list', $conditions);
        $array = array();
        $index = 0;
        foreach($results as $key => $value){
            $array[$index]['value'] = $key;
            $array[$index]['text'] = $value;
            $index++;
        }
        return json_encode($array);
    }

    /**
     * Search by user input
     * @return json
     */
    public function getResults(){
        $this->autoRender = false;

        if($this->request->is('Post')){
            $data = $this->request->data;
            $results['Business'] = $this->Business->search($data);
            $results['DomainName'] = Configure::read('DomainName');
            return json_encode(($results));
        }
    }

} 