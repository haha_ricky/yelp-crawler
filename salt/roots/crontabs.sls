crontabs:
  cron.file:
    - name: "salt://crontabs/www_cron"
    - user: "www-data"
    - require:
      - pkg: httpd
